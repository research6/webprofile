<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Jenssegers\Agent\Agent;

class FilterController extends Controller
{

	function __construct()
	{
		$this->agent = new Agent;
	}
   	public function index()
   	{
   		if ($this->agent->isMobile()) {
	   		return view('pages.filter');
   		}else{
   			abort(404);
   		}
   	}
}
