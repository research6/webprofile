@extends('layouts.master')

@section('content')
<div class="content ">
  <!-- START JUMBOTRON -->
  <div class="jumbotron" style="margin-bottom: -56px;" data-pages="parallax">
    <div class=" container-fluid   container-fixed-lg">
      <div class="inner">
        <div class="container-md-height m-b-20">
          <div class="row">
            <div class="col-xl-5 col-lg-6 col-top">
              <!-- START card -->
              <div class="card card-transparent">
                <div class="card-body">
                  <h3>Movement is supposed to be moved together!</h3>
                </p>
                <br>
                <div>
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img width="35" height="35" src="https://instagram.fpnh11-1.fna.fbcdn.net/v/t51.2885-19/s150x150/83785036_841142589644519_5570198830562934784_n.jpg?_nc_ht=instagram.fpnh11-1.fna.fbcdn.net&_nc_ohc=2VM1vk5rlwAAX8u__Rh&oh=2e9d80f656eb099dab6da690b7a34790&oe=5EC4BE2C" alt="" data-src="https://instagram.fpnh11-1.fna.fbcdn.net/v/t51.2885-19/s150x150/83785036_841142589644519_5570198830562934784_n.jpg?_nc_ht=instagram.fpnh11-1.fna.fbcdn.net&_nc_ohc=2VM1vk5rlwAAX8u__Rh&oh=2e9d80f656eb099dab6da690b7a34790&oe=5EC4BE2C" data-src-retina="https://instagram.fpnh11-1.fna.fbcdn.net/v/t51.2885-19/s150x150/83785036_841142589644519_5570198830562934784_n.jpg?_nc_ht=instagram.fpnh11-1.fna.fbcdn.net&_nc_ohc=2VM1vk5rlwAAX8u__Rh&oh=2e9d80f656eb099dab6da690b7a34790&oe=5EC4BE2C">
                    <div class="chat-status available">
                    </div>
                  </div>
                  <div class="inline m-l-10">
                    <p class="small hint-text m-t-5">Jihan Ali Reza
                      <br> Comedian</p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END card -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END JUMBOTRON -->
  <!-- START CONTAINER FLUID -->
  <div class=" container-fluid   container-fixed-lg">
    <!-- START card -->
    <div class="card card-transparent">
      <div class="card-header ">
        <div class="card-title">Instagram filter
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4">
            <!-- START card -->
            <div class="card card-transparent">
              <div class="card-body no-padding">
                <div class="view-iframe-wrapper">
                  <div class="view-port clearfix" id="chat">
                    <div class="view bg-white">
                      <!-- BEGIN View Header !-->
                      <div class="navbar navbar-default">
                        <div class="navbar-inner">
                          <div class="view-heading">
                            Filter
                            <div class="fs-11">Demo</div>
                          </div>
                        </div>
                      </div>
                      <!-- END View Header !-->
                      <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                        <div class="list-view-group-container">
                          <div class="list-view-group-header text-uppercase">
                          List</div>
                          <ul>
                            <!-- BEGIN Chat User List Item  !-->
                            <li class="chat-user-list clearfix">
                              <a data-view-animation="push-parrallax" href="https://www.instagram.com/a/r/?effect_id=211400790013972&ch=NzdiYTg3ZjZmYTM1NmRkNjYzMTcyMjUyYjhmNTZjZjY=">
                                <p class="p-l-10 col-xs-height col-middle col-xs-12 text-master">
                                  .std
                                </p>
                              </a>
                            </li>
                            <!-- END Chat User List Item  !-->
                            <!-- BEGIN Chat User List Item  !-->
                            <li class="chat-user-list clearfix">
                              <a data-view-animation="push-parrallax" href="https://www.instagram.com/a/r/?effect_id=502157903715336&ch=Yjc5YWY2ZGVjNzU2YTllYTg5ZDUyZmY0M2E0OGU4ZDk=">
                                <p class="p-l-10 col-xs-height col-middle col-xs-12 text-master">
                                  Forest
                                </p>
                              </a>
                            </li>
                            <!-- END Chat User List Item  !-->
                            <!-- BEGIN Chat User List Item  !-->
                            <li class="chat-user-list clearfix">
                              <a data-view-animation="push-parrallax" href="https://www.instagram.com/a/r/?effect_id=190621998808140&ch=MzcwNzQyODZhMzE3ZWI0NTk5NGZjMTM2MTA2N2FhYzg=">
                                <p class="p-l-10 col-xs-height col-middle col-xs-12 text-master">
                                  Reverbnation
                                </p>
                              </a>
                            </li>
                            <!-- END Chat User List Item  !-->
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END card -->
          </div>
        </div>
      </div>
    </div>
    <!-- END card -->
  </div>
  <!-- END CONTAINER FLUID -->
</div>
@endsection